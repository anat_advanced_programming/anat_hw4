#include "CryptoDevice.h"
#include "iterhash.h"
#include "pch.h"
#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <md5.h>
#include <hex.h>
#include "misc.h"
#define MY_PASSWORD "5EBE2294ECD0E0F08EAB7690D2A6EE69"//The password is: "secret"
using namespace CryptoPP;

//aquire a default key for AES and block size for CBC. can't generate randomly, since all the agents shuold have the same key.
//can initialize with a unique sequence but not needed.
CryptoPP::byte key[CryptoPP::AES::DEFAULT_KEYLENGTH], iv[CryptoPP::AES::BLOCKSIZE];
/*This function encrypts the given text
input: string plainText
output: the encrypted string
*/
std::string CryptoDevice::encryptAES(std::string plainText)
{

    std::string cipherText;

	CryptoPP::AES::Encryption aesEncryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption(aesEncryption, iv);

	CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink(cipherText));
	//  StringSink created with new does not require explicit destruction, but this is only because THIS library is build this way
	stfEncryptor.Put(reinterpret_cast<const CryptoPP::byte*>(plainText.c_str()), plainText.length() + 1);
	stfEncryptor.MessageEnd();

	return cipherText;
}
/*This function decrypts the given text
input: string cipherText
output: the decrypted string
*/
std::string CryptoDevice::decryptAES(std::string cipherText)
{

	std::string decryptedText;

	CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, iv);

	CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink(decryptedText));
	//  StringSink created with new does not require explicit destruction, but this is only because THIS library is build this way
	stfDecryptor.Put(reinterpret_cast<const CryptoPP::byte*>(cipherText.c_str()), cipherText.size());
	stfDecryptor.MessageEnd();

	return decryptedText;
}
/*This function makes the MD5 encryotion
and compares the created string to the
password the user wrote in the input
input: string message
output: the result of the comparison
*/
int CryptoDevice::MakeMD5(std::string usersPassword)
{
	int result = 0;
	byte digest[CryptoPP::Weak::MD5::DIGESTSIZE];
	//Makign the encryption to the given password
	CryptoPP::Weak::MD5 hash;
	hash.CalculateDigest(digest, (const byte*)usersPassword.c_str(), usersPassword.length());

	CryptoPP::HexEncoder encoder;
	std::string output;

	encoder.Attach(new CryptoPP::StringSink(output));
	encoder.Put(digest, sizeof(digest));
	encoder.MessageEnd();

	//Comparing the created encrypted string to out original password
	if (output == MY_PASSWORD)
	{
		result = 1;
	}
	return result;
}

