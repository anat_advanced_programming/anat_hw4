#pragma once
#include <cstdio>
#include <iostream>
#include <string.h>
#include <cstdlib>
#include "osrng.h"
#include "modes.h"
#include "iterhash.h"

class CryptoDevice
{

public:
    std::string encryptAES(std::string);
    std::string decryptAES(std::string);
	int CryptoDevice::MakeMD5(std::string usersPassword);
	

private:
    CryptoPP::byte key[CryptoPP::AES::DEFAULT_KEYLENGTH], iv[CryptoPP::AES::BLOCKSIZE];

};
